package course.spring.currencyexchangeservice;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class CurrencyExchangeController {

    private final Environment environment;
    private final ExchangeValueRepository repository;

    public CurrencyExchangeController(Environment environment, ExchangeValueRepository repository) {
        this.environment = environment;
        this.repository = repository;
    }

    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    public ExchangeValue retrieveExchangeValue(
            @PathVariable String from,
            @PathVariable String to) {
        ExchangeValue exchangeValue = repository.findByToAndFrom(to, from);
        exchangeValue.setPort(getLocalPort());
        return exchangeValue;
    }

    private int getLocalPort() {
        return Integer.parseInt(
                Objects.requireNonNull(
                        environment.getProperty("local.server.port")));
    }
}
